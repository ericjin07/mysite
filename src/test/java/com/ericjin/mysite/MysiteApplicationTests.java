package com.ericjin.mysite;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

//@RunWith(SpringRunner.class)
@SpringBootTest
public class MysiteApplicationTests {

    @Test
    public void mapView() {
        Map<String,String> map = new LinkedHashMap<>();
        map.putIfAbsent("1","kvallrfsda");
        map.putIfAbsent("2","kvallr24");
        map.putIfAbsent("3","kvafsdfwerllr");
        map.putIfAbsent("4","kfsvallr");
        Map sycn = Collections.synchronizedMap(map);

        Set<String> teminalId = new HashSet<>();
        teminalId.add("1");
        map.keySet().removeAll(teminalId);

//entry key  ,return a Set of Map.Entry  the interface
        for (Map.Entry<String ,String> entry:map.entrySet()){
            System.out.println(entry.getKey() + "---" + entry.getValue());
        }

        map.forEach((k,v) -> System.out.println(k + "|||||| " + v));

        for (String key:map.keySet()){
            System.out.println(key + "===" + map.get(key));
        }

        for (String value: map.values()){
            System.out.println(value + "++++value");
        }
    }

    @Test
    public void weakHashMap(){
//        WeakHashMap
        String[] arr = {"123","31"};
        //a array to a Collection.  not just Arrays.asList.-> it's unmodified
        List<String> list = new ArrayList<>(Arrays.asList(arr));
        list.add("31");
        System.out.println("Arr2Col" + list);

        //a Collection to Array
        String[] colToArr = list.toArray(new String[0]);
        System.out.println("col2Arr  " + Arrays.toString(colToArr));

        List<String> arrlist = new ArrayList<String>();
//        arrlist = Collections.checkedList((List<String>) arrlist,String.class);
//        arrlist.add(new Date());
        for (String st : arrlist){
            System.out.println(st);
        }
    }

    @Test
    public void shuffleTest(){
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0;i<=49;i++) numbers.add(i);
        Collections.shuffle(numbers);
        System.out.println(numbers);
        int i = Collections.binarySearch(numbers,7);
        System.out.println(i);
        List<Integer> winningCombination = numbers.subList(0,6);
        Collections.sort(winningCombination);
        System.out.println(winningCombination);
        numbers.retainAll(winningCombination);
        System.out.println(numbers);
        Hashtable ht = new Hashtable();
        ht.put("fsdf","fsadf");
        Enumeration en = ht.elements();
        while (en.hasMoreElements()){
            System.out.println(en.nextElement());
        }

        //stack
    }

}
