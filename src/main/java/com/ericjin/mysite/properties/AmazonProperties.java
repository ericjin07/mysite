package com.ericjin.mysite.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Create by IntelliJ IDEA.
 * Author: EricJin
 * Date: 07/22/2018 17:02
 */
@Component
@ConfigurationProperties(prefix = "amazon")
public class AmazonProperties {
    private String associateID;

    public String getAssociateID() {
        return associateID;
    }

    public void setAssociateID(String associateID) {
        this.associateID = associateID;
    }
}
