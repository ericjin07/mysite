package com.ericjin.mysite.repository;

import com.ericjin.mysite.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Create by IntelliJ IDEA.
 * Author: EricJin
 * Date: 07/21/2018 12:52
 */
public interface ReadingListRepository extends JpaRepository<Book,Long> {

    List<Book> findByReader(String reader);

}
