package com.ericjin.mysite.repository;

import com.ericjin.mysite.entity.Reader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Create by IntelliJ IDEA.
 * Author: EricJin
 * Date: 07/22/2018 13:54
 */
@Repository
public interface ReaderRepository extends JpaRepository<Reader,String> {

    Reader findByUsername(String username);

}
